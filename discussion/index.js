// keywords- here we can identify the certain keywords.

//GOAL: create a server using 'Express.js'

//1. Use the require() directive to acquire the express library and its utilities
	const express = require('express');

//2. Create a server using only express.

	const server = express();
	//express will allow us to create an express application.

//3. Identify a port/address that will serve/host newly createdd connection/pp.
	const address = 3000;

//4. Bind the application to the desired designated port using the listen(), create a fucntion or a mthod that will display a response that the cconnection has been establish

	server.listen(address, () => {
		console.log(`Server is running on port: ${address}`)
	});